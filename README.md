# STM32 Blue Pill

On this tutorial, it is explained how to program the Blue Pill ( STM32F103C8T6 mcu) using FTDI and Arduino IDE. 
Furthermore, it explains how to program the Blue Pill using the HAL library and STM32CubeIDE and upload the code using FTDI (without st-link programmer).


[![IMG-20200315-163827.jpg](https://i.postimg.cc/kGKmxBf3/IMG-20200315-163827.jpg)](https://postimg.cc/Fkr8vFMD)



Usefull link:
https://stm32f4-discovery.net/2017/07/stm32-tutorial-efficiently-receive-uart-data-using-dma/

gitlab: https://github.com/MaJerle/stm32-usart-uart-dma-rx-tx
